package com.owen.unittestsample.calculation;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class CalculationManagerTest {
    private ICalculation mCalculationManager;

    @Before
    public void setUp() throws Exception {
        //Target
        mCalculationManager = new CalculationManager();
    }

    @Test
    public void testAdd() {
        int actual = mCalculationManager.add(1, 1);
        int excepted = 2;
        Assert.assertEquals(excepted, actual); //true 實際結果是否與期望值相符
    }

    @Test
    public void testMinus() {
        //正數相減
        Assert.assertEquals(0, mCalculationManager.minus(1, 1)); //true
        //正負數相減
        Assert.assertEquals(3, mCalculationManager.minus(1, -2)); //true
        //自然數相減
        Assert.assertEquals(0, mCalculationManager.minus(0, 0)); //true
    }
}