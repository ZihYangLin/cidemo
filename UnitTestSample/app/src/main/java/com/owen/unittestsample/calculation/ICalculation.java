package com.owen.unittestsample.calculation;

public interface ICalculation {
    int add(int a, int b);

    int minus(int a, int b);
}