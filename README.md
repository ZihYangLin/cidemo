## 持續整合(Continuous Integration, CI)


### 目錄
* [簡述](#簡述)
* [版本控制](#版本控制)
    * [集中式系統](#集中式系統(SVN))
    * [分散式系統](#分散式系統(Git))
* [程式架構](#程式架構)
    * [View層](#View層)
    * [數據層](#數據層)
    * [業務邏輯層](#業務邏輯層)
* [單元測試](#單元測試)

# 簡述
持續整合的目的，利用頻繁地提交新功能的變更，觸發自動化建置和測試，確保最新版本的軟體是可運行的。
* [版本控制](#版本控制): 沒有版控，就沒有 CI/CD。
* [建置](#程式架構): 確保提交的程式碼是否可以執行的。
* 自動化測試: 確保功能正常與軟體品質。
* 程式碼分析: 檢查 code style 或程式的穩健度。

# 版本控制
文件控制（documentation control），能記錄任何工程專案內各個模組的改動歷程，並為每次改動編上序號。
* [集中式系統](#集中式系統(SVN))
* [分散式系統](#分散式系統(Git))

### 集中式系統(SVN)

專案程式碼存放在中央伺服器，需從中央伺服器下載程式碼修改，
修改後提交回中央伺服器作管理。

	1. 每個版本庫只有唯一版本。
	2. 提交必須連網，無Local版本概念。
	3. 提交需要授權，無許可權不可提交。
    4. 提交是有順序的，後者必須解決與前者的衝突。

### 分散式系統(Git)

又稱去中心化版本控制，是一種版本控制的方式，它允許軟體開發者可以共同參與一個軟體開發專案，但是不必在相同的網路系統下工作。

	1. 可以從任何版本Clone建立屬於自己的版本庫。
	2. 本地提交(commit)無需授權。
    3. 合併時需解決衝突問題，無先後順序。

# 程式架構
Android裡大部分的程式碼的業務邏輯與View有著高耦合特性，必須先從這裡做關注點分離(SoC)。

在系統架構中可廣義分為三層，這樣有利於系統的開發、維護、部署和擴展。

* [View層](#View層)
* [數據層](#數據層)
* [業務邏輯層](#業務邏輯層)

### View層
View層顧名思義就是對於Layout的繪製控制，以及使用者互動行為。
* onClickListener
* onTouch
* setText
* etc …

### 數據層
數據層掌控資料讀取及來源，內容包含網路、資料庫、檔案等等。資料傳輸介面抽象化，可讓使用上更加靈活。在單元測試內扮演著固定的角色，必須抽離所有不可控因素，因此數據層都會是假資料模組供邏輯測試使用。

### 業務邏輯層
業務邏輯層主要是View層與數據層重要的中間層，[單元測試](#單元測試)主要就是測試這層的邏輯是否有誤，要符合測試需要幾個特性。

* 單一職責原則 (Input, Output)
* 獨立性，不依賴外部資源

# 單元測試
程式碼需要可被測試化，程式單元是應用的最小可測試部件，一個單元就是單個程式、函式、過程等；對於物件導向程式設計，最小單元就是方法，包括基礎類別、抽象類、或者衍生類別（子類）中的方法，且只對public、default的方法測試，因為private的方法只會在類內部調用，也視為邏輯的一部分。

單元測試應該具備的特性：
* 最小測試單位
* 外部相依性為零

>檔案讀取不到、網路斷線、第三方套件錯誤都不應該影響到這個測試的結果，只有程式錯誤才應該影響測試結果。

測試程式不具有任何業務邏輯(if else, switch case, try catch, etc.)，需符合最小測試單元特性。

測試程式有三個要素組成
* target: 測試目標 (通常是Class)
* actual: 實際結果
* excepted: 期望值

Sample Code:
```java
interface ICalculation {
    int add(int a, int b);

    int minus(int a, int b);
}

class class CalculationManager implements ICalculation {

    @Override
    int add(int a, int b) {
        return a + b;
    }

    @Override
    int minus(int a, int b) {
        return a - b;
    }
}
```

Test Code:
```java
public class CalculationManagerTest {
    private ICalculation mCalculationManager;

    @Before
    public void setUp() throws Exception {
        //Target
        mCalculationManager = new CalculationManager();
    }

    @Test
    public void testAdd() {
        int actual = mCalculationManager.add(1, 1);
        int excepted = 2;
        Assert.assertEquals(excepted, actual); //true 實際結果是否與期望值相符
    }

    @Test
    public void testMinus() {
        //正數相減
        Assert.assertEquals(0, mCalculationManager.minus(1, 1)); //true
        //正負數相減
        Assert.assertEquals(3, mCalculationManager.minus(1, -2)); //true
        //自然數相減
        Assert.assertEquals(0, mCalculationManager.minus(0, 0)); //true
    }
}
```

#### 執行測試
* Run 'CalculationManagerTest'      
* Debug 'CalculationManagerTest'    
* Run 'CalculationManagerTest' with Coverage  //[測試覆蓋率](#測試覆蓋率)

!["執行測試"](./images/001.png)

#### 測試結果
成功：測試結果應該都如此，ALL PASS !
!["測試結果成功"](./images/002.png)
失敗：不應該測試失敗案例（這是為了展示用的）
!["測試結果失敗"](./images/004.png)

#### 測試覆蓋率
!["測試覆蓋率"](./images/003.png)